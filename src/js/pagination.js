const pageButtons = document.getElementsByClassName("pagination-numButton");
const leftPageBtn = document.getElementsByClassName('page-to-left-btn');
const rightPageBtn = document.getElementsByClassName('page-to-right-btn');
const articlePages = document.getElementsByClassName('article-page');
const pageNumText = document.getElementsByClassName('pageText');
const firstPageBtn = document.getElementsByClassName('firstPage');
const secondPageBtn = document.getElementsByClassName('secondPage');
const thirdPageBtn = document.getElementsByClassName('thirdPage');
let currentPage = 0;

[...pageButtons].forEach((button) => {
    button.addEventListener('click', () => {
        let previousPage = currentPage;
        currentPage = button.getAttribute('data-page');
        changePage(currentPage, previousPage);
    })
});

[...leftPageBtn].forEach((button) => {
    button.addEventListener('click', () => {
        let previousPage = currentPage;
        currentPage--;
        if (currentPage < 0) currentPage = 2;
        changePage(currentPage, previousPage);
    })
});

[...rightPageBtn].forEach((button) => {
    button.addEventListener('click', () => {
        let previousPage = currentPage;
        currentPage++;
        if (currentPage > 2) currentPage = 0;
        changePage(currentPage, previousPage);
    })
});

const changePage = (index, previousPage) => {
    [...articlePages].forEach((page) => {
        page.style.cssText = 'display:none !important';
    });
    articlePages[index].style.cssText = 'display:flex !important';
    changePageText();
    changeActiveBtn(previousPage);
}

const changePageText = () => {
    [...pageNumText].forEach(text => {
        text.innerHTML = 'Page ' + ((Number(currentPage)) + 1) + ' of 3';
    })
}

const changeActiveBtn = (previousPage) => {
    switch (Number(previousPage)) {
        case 0: {
            [...firstPageBtn].forEach(btn => {
                btn.classList.remove('active');
            });
            break;
        }
        case 1: {
            [...secondPageBtn].forEach(btn => {
                btn.classList.remove('active');
            });
            break;
        }
        case 2: {
            [...thirdPageBtn].forEach(btn => {
                btn.classList.remove('active');
            });
            break;
        }
    }
    switch (Number(currentPage)) {
        case 0: {
            [...firstPageBtn].forEach(btn => {
                btn.classList.add('active');
            });
            break;
        }
        case 1: {
            [...secondPageBtn].forEach(btn => {
                btn.classList.add('active');
            });
            break;
        }
        case 2: {
            [...thirdPageBtn].forEach(btn => {
                btn.classList.add('active');
            });
            break;
        }
        default: console.log("troll");
    }
}


changePage(currentPage);